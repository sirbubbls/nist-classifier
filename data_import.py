import numpy as np
from skimage.io import imread
from os import listdir, scandir

path_image = './dataset/data/images/'

"""
Import Images
"""

image_classes = listdir(path_image)
n_classes = len(image_classes)


def get_n_classes():
    return n_classes


def get_batch(requested_points):
    X = []
    y = []

    classes = [f.name for f in scandir(path_image) if f.is_dir()]

    point_per_class = requested_points // len(classes)

    for class_id in range(n_classes):
        class_name = classes[class_id]
        image_path = path_image + class_name + '/train_' + class_name + '/'
        images = listdir(image_path)

        data_size = len(images)

        start = int(np.random.randint(data_size - point_per_class))
        end = int(start + point_per_class)

        for image in images[start:end]:
            X.append(imread(image_path + image, as_gray=True))
            y.append(class_id)

    for num in range(requested_points - len(y)):
        X.append(X[num])
        y.append(y[num])

    s = np.arange(len(y))
    np.random.shuffle(s)

    X = np.array(X)
    y = np.array(y).astype(np.int32)

    X = X[s]
    y = y[s]

    s = np.random.randint(0, len(y), requested_points - len(y))

    X = X.reshape((-1, 128, 128, 1)).astype(np.float32)
    return X, y
