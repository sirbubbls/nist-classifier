from tqdm import tqdm
import time
import sys
import time


class TensorBar:
    def __init__(self, total):
        self.bar_length = 50
        self.total = total
        self.current = 0
        self.loss = -1
        self.loss_history = []
        self.accuracy = 0
        self.c_acc = 0
        self.last_update = 0
        self.current_update = 0

    def update_time(self):
        self.last_update = self.current_update
        self.current_update = time.process_time_ns() / 1000000

    def progress(self, progress):
        self.update_time()
        self.current += progress

    def update(self, progress, loss, accuracy):
        self.update_time()
        self.loss = loss
        self.accuracy = accuracy
        self.current += progress

    def print(self):
        sys.stdout.write("\r")

        # Front
        sys.stdout.write('\033[;1m')
        sys.stdout.write("(" + str(self.current) + "/" + str(self.total) + ")")

        percentage = int((self.current / self.total) * self.bar_length)

        sys.stdout.write('\033[1;31m [')

        for i in range(percentage):
            sys.stdout.write("=")

        for i in range(self.bar_length - percentage):
            sys.stdout.write('-')

        sys.stdout.write('] ')

        # Coloring of Percentage

        if self.accuracy >= .75:
            sys.stdout.write('\033[0;32m')
        elif self.accuracy < .75 and self.accuracy >= .5:
            sys.stdout.write('\033[1;34m')
        elif self.accuracy > .25 and self.accuracy < .5:
            sys.stdout.write('\033[0;35m')
        else:
            sys.stdout.write('\033[1;31m')

        sys.stdout.write('[' + str(int(self.accuracy * 100)) + ' %]')

        sys.stdout.write('\033[0;0m')

        if self.loss != -1:
            sys.stdout.write(" Loss: " + str(self.loss))

        sys.stdout.write(' TDelta: ' + str((self.current_update - self.last_update).__round__(2)) + 's')

        sys.stdout.write(" ")
        sys.stdout.flush()



# bar = TensorBar(10)

# for i in range(10):
#     bar.update(1, .4, 0.8)

#     time.sleep(0.4)

#     bar.print()
