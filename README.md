# NIST-Classifier
I learned working with slightly larger images than for example MNIST image sizes using Convolution and Pooling Layers.

# Current DNN Layout
|Nr.|Type|Padding|Activation|Size|
|---|---|---|---|---|
|1.|Convolution Layer|SAME|ReLU| |
|2.|Pooling Layer|VALID| | |
|3.|Flatten| | | |
|4.|Dropout (0.1)| | | |
|5.|Dense Layer| |ReLU|512|
|6.|Dense Layer| |ReLU|256|
|7.|Logits(Output)| | |[n_classes]|


# Dataset
**Nist Special Database**
Handwritten letters and numbers.

https://www.nist.gov/srd/nist-special-database-19
(**DL Link**: https://s3.amazonaws.com/nist-srd/SD19/by_class.zip)

## Stats
**Number of Classes:** 62
**Input:** Grayscale Images with Dimensions 128*128
