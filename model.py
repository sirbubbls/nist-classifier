from data_import import get_batch, get_n_classes
from dependencies.TensorViz import TensorBar
from datetime import datetime
import tensorflow as tf
import numpy as np
import sys

now = datetime.utcnow().strftime("Y%m%d%H%M%S")
logdir = 'logs/run-{}/'.format(now)


n_classes = get_n_classes()
print(n_classes)

X = tf.placeholder(tf.float32, shape=(None, 128, 128, 1), name='X')
y = tf.placeholder(tf.int64, shape=(None), name='y')

# Layer 1
with tf.name_scope('Layer1'):
    conv1 = tf.layers.conv2d(X, 16, 6, activation=tf.nn.relu, padding='SAME')
    # pool1 = tf.layers.max_pooling2d(conv1, 2, 2, padding='VALID')

with tf.name_scope('Layer2'):
    conv2 = tf.layers.conv2d(conv1, 32, 3, activation=tf.nn.relu, padding='SAME')
    pool2 = tf.layers.max_pooling2d(conv2, 2, 2, padding='VALID')

with tf.name_scope('Layer3'):
    conv3 = tf.layers.conv2d(pool2, 48, 3, activation=tf.nn.relu, padding='SAME')
    pool3 = tf.layers.max_pooling2d(conv3, 2, 2, padding='VALID')

with tf.name_scope('FlattenLayer'):
    flatten = tf.layers.flatten(pool3)
    dropout = tf.layers.dropout(flatten, 0.1)

with tf.name_scope('DenseLayers'):
    dense1 = tf.layers.dense(dropout, 256, activation=tf.nn.relu)
    dense2 = tf.layers.dense(dense1, 128, activation=tf.nn.relu)

with tf.name_scope('OutputLayer'):
    logits = tf.layers.dense(dense2, n_classes, name='logits')

with tf.name_scope('loss'):
    loss = tf.losses.sparse_softmax_cross_entropy(labels=y, logits=logits)

with tf.name_scope('train'):
    optimizer = tf.train.AdamOptimizer(0.001)
    training_op = optimizer.minimize(loss)

with tf.name_scope('eval'):
    correct = tf.nn.in_top_k(logits, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

n_epochs = int(sys.argv[3])

init = tf.global_variables_initializer()
file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())
loss_summary = tf.summary.scalar('Loss', loss)

batch_size = int(sys.argv[1])
iterations = int(sys.argv[2])
restore = False

saver = tf.train.Saver()
step = np.int64(0)
with tf.Session() as sess:
    if restore:
        try:
            saver.restore(sess, './model/v2.ckpt')
            print("Loaded...")
        except:
            init.run()
    else:
        init.run()

    viz = TensorBar(n_epochs)
    for epoch in range(n_epochs):
        X_batch, y_batch = get_batch(batch_size)
        for i in range(iterations):
            sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
            step = batch_size + step
        eval_loss = loss_summary.eval(feed_dict={X: X_batch[:batch_size], y: y_batch[:batch_size]})
        file_writer.add_summary(eval_loss, step)

        viz_loss = loss.eval(feed_dict={X: X_batch[:100], y: y_batch[:100]})
        viz_acc = accuracy.eval(feed_dict={X: X_batch[:100], y: y_batch[:100]})
        viz.update(1, viz_loss, viz_acc)
        viz.print()
        saver.save(sess, './model/v2.ckpt')
